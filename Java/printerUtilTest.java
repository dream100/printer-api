package com.cn.news.util;



import java.io.IOException;
import java.util.Date;
import org.apache.http.client.ClientProtocolException;
import org.junit.jupiter.api.Test;
import com.cn.news.bean.Printer;
import java.util.HashMap;
import org.json.JSONObject;



class printerUtilTest {
	@Test
	void test() throws ClientProtocolException, IOException {
        String url = "https://open-api.ushengyun.com/printer/print";
		Printer printer = new Printer();
		int time =Math.round(new Date().getTime()/1000);//时间戳
		String data = "测试";//打印内容
		printer.setAppid("****");// Your appid
		printer.setAppsecret("");// Your appsecret
		printer.setDeviceid("");//设备id
		printer.setDevicesecret("");//设备秘钥
		printer.setTimestamp(time);//时间戳
		printer.setData(data);
		String sign = printerUtil.getSign(printer);
		System.out.println("time："+time);
		System.out.println("url："+printerUtil.percentEncode(data));
		System.out.println("sign："+sign);
		//请求数据
        HashMap<String, Object> req = new HashMap<String, Object>();

        req.put("appid",printer.getAppid());
        req.put("sign",sign);
        req.put("timestamp",time);
        req.put("deviceid",printer.getDeviceid());
        req.put("devicesecret",printer.getDevicesecret());
        req.put("printdata",data);
        JSONObject jsonObj = new JSONObject(req);//转化为json格式 
		//String datas = "appid="+printer.getAppid()+"&sign="+sign+"&timestamp="+time+"&deviceid="+ printer.getDeviceid()+"&devicesecret="+printer.getDevicesecret()+"&printdata="+printerUtil.percentEncode(data);
        System.out.println(jsonObj.toString());
		printerUtil.sendPost(url, jsonObj.toString());
	}

}

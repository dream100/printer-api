# 优声云打印机PHP SDK 使用教程
## 安装
`composer install ushengyun\printer`
如果安装不上，可直接下载
## 示例
* 打印
```php

$app_id = '******';

$app_secret = '********';

$rpc = new \ushengyun\protocol\RpcClient($app_id, $app_secret, 'https://open-api.ushengyun.com/printer');

$Zprinter = new \ushengyun\Printer($rpc);

$device_id = '1111111';

$device_secret = '11111111';

$printdata = '优声云拥有自主研发的云打印机，提供稳定高效，高可用的云打印方案';

try {

    $Zprinter->set_args($device_id, $device_secret)->cloud_print($printdata);
    
} catch (Exception $e) {

}
```
* 设置声音
```php
<?php

$app_id = '******';

$app_secret = '********';

$rpc = new \ushengyun\protocol\RpcClient($app_id, $app_secret, 'https://open-api.ushengyun.com/printer');

$Zprinter = new \ushengyun\Printer($rpc);

$device_id = '1111111';

$device_secret = '11111111';

try {

    $Zprinter->set_args($device_id, $device_secret)->set_sound(1);

} catch (Exception $e) {

}
```